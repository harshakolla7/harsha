$resource = "Desktop-0GUMLBC"
$source = "SIT"
$work = 'C:\BluePrism\Newfolder\Testing'
$Data = $env:Data
$output = $work + "\" + "output.txt"

Get-Content $Data | ForEach-Object {
    Start-Process -FilePath "C:\Program Files\Blue Prism Limited\Blue Prism Automate\automatec.exe" -wait -Passthru -WorkingDirectory "$work" -ArgumentList "/resource $resource /user admin admin123 /dbconname $source /setev $_ " -RedirectStandardOutput $output
}
